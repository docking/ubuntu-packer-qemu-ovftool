# ubuntu-packer-qemu-ovftool

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04
ARG PACKER_VERSION=1.6.6


FROM ${DOCKER_REGISTRY_URL}ubuntu-packer-qemu:${CUSTOM_VERSION}-${UBUNTU_VERSION}-${PACKER_VERSION} AS ubuntu-packer-qemu-ovftool

ARG OVFTOOL_BUNDLE=VMware-ovftool-4.4.1-16812187-lin.x86_64.bundle

ENV \
	DEBIAN_FRONTEND=noninteractive \
	container=docker

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
			locales \
			; \
	apt-get -q -y clean ;

RUN \
	locale-gen en_US.UTF-8 ; \
	update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

WORKDIR /tmp

COPY \
	./tools/ \
	./

RUN \
	/bin/sh ./${OVFTOOL_BUNDLE} --console --eulas-agreed --required ; \
	rm ./${OVFTOOL_BUNDLE}

WORKDIR /

RUN \
	ovftool -v
